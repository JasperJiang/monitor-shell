#!/bin/bash
 PATH=/bin:/sbin:/usr/bin:/usr/sbin/:/usr/local/bin:/usr/local/sbin:~/bin
  export PATH
DATE=`date +%F`
password="bc29b36f"
LOG=/home/kakali.band/autoshell/log/allcommand.log
LOG2=/home/kakali.band/autoshell/log/error.log
BACKUP=/DATA/dumps/mongodata/mongodbdump
SPATH=/DATA/dumps/mongodata/ViQBetaStaticFiles
WHICHJSON=/home/kakali.band/autoshell/log/json.txt
FPATH=/home/pwc
FBPATH=/home/pwc/history_backup
APIPATH=/var/pwc-importer
VIRPATH=/var/virtualenvs/pwc-importer
WEBPATH=/var/www/pwc
FLAG='0'
#ECHO LOG
echo "<b>DATE: $DATE</b>" >$LOG2
#echo "======================$DATE=====================" >$LOG
# exec 1>$LOG
exec 2>>$LOG2

#UPDATE API FROM GETb
cd $APIPATH
cp $APIPATH/pwc_importer/settings.py $SPATH/settings.py
/usr/expect/bin/expect -c "spawn git fetch
set timeout 3000
expect \":*\"
send \"sjohnson131\r\"
expect \":*\"
send \"viqapp123\r\"      
expect \"]$\"
"
git checkout viq-dev
git commit -a -m 'COMMITBYSHELL'
/usr/expect/bin/expect -c "spawn git pull --no-edit
set timeout 3000
expect \":*\"
send \"sjohnson131\r\"
expect \":*\"
send \"viqapp123\r\"    
expect \"]$\"
"
cp $SPATH/settings.py $APIPATH/pwc_importer/settings.py

cd $WEBPATH
/usr/expect/bin/expect -c "spawn git fetch --no-edit
set timeout 3000
expect \":*\"
send \"sjohnson131\r\"
expect \":*\"
send \"viqapp123\r\"      
expect \"]$\"
"
git checkout staging5-www
/usr/expect/bin/expect -c "spawn git pull
set timeout 3000
expect \":*\"
send \"sjohnson131\r\"
expect \":*\"
send \"viqapp123\r\"      
expect \"]$\"
"

echo "<b>CHECK UPDATE_TIME ERROR:</b>">>$LOG2
/usr/expect/bin/expect -c "spawn sftp  pwc@api.viqbeta.com
set timeout 3000
expect {
        \"(yes/no)?\"
                {
                        send \"yes\n\"
                        expect \"*assword:\" {send \"$password\n\"}
                }
        \"*assword:\"
                {
                        send \"$password\n\"
                }

}
expect \">\"
send \"lcd $SPATH\n\"
expect \">\"
send \"get update_time.txt\n\"
expect \">\"
send \"bye\n\"
expect \"$\"
"
read IMPORTTIME < $SPATH/import_time.txt

read UPDATTIME < $SPATH/update_time.txt

# CHECK IMPORT TIME
# if [ "$IMPORTTIME"X \< "$UPDATTIME"X ]
if true
then
FLAG='1'
echo "<b>FTP ERROR:</b>">>$LOG2
/usr/expect/bin/expect -c "spawn sftp  pwc@api.viqbeta.com
set timeout 3000
expect {
        \"(yes/no)?\"
                {
                        send \"yes\n\"
                        expect \"*assword:\" {send \"$password\n\"}
                }
        \"*assword:\"
                {
                        send \"$password\n\"
                }

}
expect \">\"
send \"cd $FPATH/data/\n\"
expect \">\"
send \"lcd $SPATH/data/\n\"
expect \">\"
send \"get *.json\n\"
expect \">\"
send \"cd $FPATH\n\"
expect \">\"
send \"lcd $SPATH\n\"
expect \">\"
send \"get -r PDF\n\"
expect \">\"
send \"get -r training_materials\n\"
expect \">\"
send \"bye\n\"
expect \"$\"
"
echo "$DATE" > $SPATH/import_time.txt
mv $SPATH/data/adjustmenttype.json $SPATH/adjustmenttype.json
echo "<b>IMPORT ERROR:</b>">>$LOG2

find $SPATH/data/ -name '*.json' -exec basename {} \;  > $WHICHJSON
 while read line
do
	# mongoimport --db viq_data --collection ${line%.json} --file $SPATH/data/$line --upsert --batchSize 100
    mongoimport --host 172.21.99.45:27017 --db viq_data --collection ${line%.json} --file $SPATH/data/$line --batchSize 100
done < $WHICHJSON
rm -rf $WHICHJSON

rm -f $SPATH/data/*

/usr/expect/bin/expect -c "spawn mongo 172.21.99.45:27017 
set timeout 3000
expect \">\"
send \"db.pvp.createIndex({'ticker':1, 'valens_year':1, 'year_code':1});db.pvp.createIndex({'ticker':1, 'scenario':1, 'year_code':1});db.pvp.createIndex({'ticker':1, 'year_code':1});\r\"
expect \">\"
send \"db.pwc.createIndex({'ticker':1, 'valens_year':1, 'year_code':1});db.pwc.createIndex({'ticker':1, 'scenario':1, 'year_code':1});db.pwc.createIndex({'ticker':1, 'year_code':1});\r\"      
expect \">\"
send \"db.wacc.createIndex({'ticker':1, 'valens_year':1, 'year_code':1});db.wacc.createIndex({'ticker':1, 'scenario':1, 'year_code':1});db.wacc.createIndex({'ticker':1, 'year_code':1});\r\"
expect \">\"
send \"db.shareholder.createIndex({'ticker':1, 'data_section': 1});db.stock_price.createIndex({'ticker': 1, 'data_date': 1, 'data_point': 1});\r\"
expect \">\"
send \"db.revenue.createIndex({'ticker': 1, 'data_section': 1, 'data_point': 1, 'year_code': 1, 'valens_year': 1});\r\"
expect \">\"
send \"db.complex_data.createIndex({'ticker':1, 'valens_year':1, 'year_code':1});db.complex_data.createIndex({'ticker':1, 'scenario':1, 'year_code':1});db.complex_data.createIndex({'ticker':1, 'year_code':1});\r\"
expect \">\"
send \"db.company.createIndex({'ticker': 'text', 'name': 'text'});db.index.createIndex({'data_point': 1, 'data_date': 1, 'scenario': 1});db.index.createIndex({'data_point': 1, 'data_date': 1})\r\"
expect \">\"
send \"exit\r\"
expect \"$\"
"
#RUN POST-IMPORT SCRIPTS
supervisorctl restart pwcapi:
cd $APIPATH
source $VIRPATH/bin/activate
$VIRPATH/bin/python3.5 manage.py loaddata $SPATH/adjustmenttype.json
rm -f $SPATH/adjustmenttype.json
export PYTHONPATH=$PYTHONPATH:$PWD
nohup $VIRPATH/bin/python3.5 mongo_exporter/export_company_data.py > export_ffg.log &
deactivate
#CLEAR PHP CACHE
cd $WEBPATH
php artisan cache:clear


#MANAGE PDF REPORTS
mount --bind $SPATH/PDF /PwCFiles/PDF
chmod -R 777 /PwCFiles/PDF
cd $APIPATH
source $VIRPATH/bin/activate
export PYTHONPATH=$PYTHONPATH:$PWD
$VIRPATH/bin/python3.5 importer/console_scripts/import_pdf.py
deactivate

#MANAGES THE TRAINING VIDEOS
cd $WEBPATH
mount --bind $SPATH/training_materials $WEBPATH/training_materials
chmod -R 777 training_materials

#PRE-CACHING API CALLS
# cd $APIPATH
# source $VIRPATH/bin/activate
# export PYTHONPATH=$PYTHONPATH:$PWD
# export DJANGO_SETTINGS_MODULE=pwc_importer.settings
# nohup $VIRPATH/bin/python3.5 pre_cache.py --username admin --password Pwcwelcome1 > api_precaching.log &
# deactivate


else
    echo "<b>THE DATA IS THE LATEST</b>">>$LOG2
fi

python /home/kakali.band/autoshell/sendmail.py $FLAG
